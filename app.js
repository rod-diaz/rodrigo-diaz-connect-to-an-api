
//Variables
const apiKey = "55dcbc02"; //Use enviroment variables
const verbose = document.getElementById("verbose");
const seachLbl = document.getElementById("searchLbl");
const searchBtn = document.getElementById("searchBtn");
const movieCard = document.getElementById("movieCard");
const cardTitle = document.getElementById("cardTitle");
const cardDescription = document.getElementById("cardDescription");
const cardElements = document.getElementById("cardElements");
const messageError = document.getElementById("messageError");

//functions

searchBtn.addEventListener("click", function(e){
    e.preventDefault();

    let movie = seachLbl.value;    
    let url = "http://www.omdbapi.com/?apikey=" + apiKey + "&t=" + movie;
    
    getMovie(url);
});


async function getMovie(url){
    
    seachLbl.value = "";
    cardElements.getElementsByTagName('li')[1].style.display = "none";

    let response = await fetch(url);

    if (response.status === 200) {

        verbose.style.display = "none";
        let body = await response.json();

        if(!body.Error){
            createMovieCard(body);
        } else{
            movieCard.style.display = 'none';
            messageError.style.display = "block"
            setTimeout(function(){ 
                messageError.style.display = "none"
                verbose.style.display = "block";
            }, 4000);
            console.log(body.Error);
        } 
    }
}

function createMovieCard(body){
    console.log(body);
    
    movieCard.getElementsByTagName('img')[0].src = body.Poster; 
    cardTitle.innerHTML = body.Title;
    cardDescription.innerHTML = body.Plot;
    cardElements.getElementsByTagName('li')[0].innerHTML = "Genre: " + body.Genre;
    
    if(body.Type !== "movie"){
        cardElements.getElementsByTagName('li')[1].style.display = "block";
        cardElements.getElementsByTagName('li')[1].innerHTML = "Seasons: " + body.totalSeasons;
    }
    cardElements.getElementsByTagName('li')[2].innerHTML = "Actors: " + body.Actors;

    movieCard.style.display = 'block';
}

